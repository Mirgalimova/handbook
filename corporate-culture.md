# No micromanagement
Avoid micromanagement

* Practice of autonomous work with probably a little bit far from *ideal* result is better than *ideal* but with micromanagement

# No rushing

Super urgent tasks is a sign of bad planning

* After each case rethink what was the reason and how to avoid it

# Respectful communication

* Don't use rhetorical questions. They don't help to analyze a problem and move forward.
* Don't use pronouns (*he*, *she*). At least when the person can hear what you said or read what you wrote
* Replace word "Начальник" with "Руководитель" whenever you want to use the first one
* Never use unrespectful gestures: [example](images/gesture.gif)
* Не используй в речи приставку "же" (и близкие), если она используется в качестве выражения недовольства или агрессии, например

  * "мы же договорились"
  * "ты же говорил"
  * "я же сказал" 
  * "я ведь сказал"
  * "я уже говорил"

# Be open for community

* twitter

    * you can use your account to tweet about development, work, traveling, vacation, fun
    * twitter profile's website URL has to be your page at team section, e.g. it-projects.info/team/yelizariev
    * no requirements for profile description, header photo
    * profile photo should be a real face photo
    * location has to be specifed ``YouCity, Country``

* github

    * https://github.com/settings/profile

      * public email: unselected
      * URL must be link to public profile at company's website
      * Company must be set to ``@it-projects-llc``
      * Location must be your ``YouCity, Country``
      * Photo must be your real face photo

    * https://github.com/settings/emails

      * primary email must be personal address …@it-projects.info
      * “Keep my email address private” must be switched off

    * https://github.com/orgs/it-projects-llc/people

      * get invitation
      * set ``Organization visibility`` to ``Public``
    * at your computer execute following command:
    
            git config --global user.email "YOUR_WORK_EMAIL@it-projects.info"
    
    * https://gitlab.com/profile -- fill the following fields
    
      * Twitter
      * Website -- URL to public profile at company's website
      * Location -- ``YouCity, Country``
      * Organization -- IT-Projects LLC
      
* https://gravatar.com

  * Be sure that your company email has avatar
