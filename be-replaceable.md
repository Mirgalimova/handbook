Being replaceable is not a bad thing. It's a good thing. 

* https://www.inc.com/joe-hyrkin/3-reasons-why-being-replaceable-will-help-you-grow-in-your-career.html
* https://www.youtube.com/watch?v=yRm97umW4vE&list=PLaIsQH4uc08z1_vgtlWfPbV0DL2L7exGp

# Document everything

You must make instructions and documentation for everything that you can forget or may need to pass to your colleagues.

It's never a question do you have time to make documentation or not. 
It's a question whether it will be done or not, because
it's possible to postpone the documentations infinitely. 
If you really don't have time, that may mean that you took too much responsibilities or, 
in some cases, you would need to delegate writing proper docs.

# For developers: make maintainable code

This will help your colleagues to update your code in future

* write comments on a complicated part of code
* write tests